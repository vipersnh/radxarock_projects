#define GRF_BASE_ADDR 0x20008000
#define GPIO0_DIR_OFFSET 0x00
#define GPIO0_DO_OFFSET 0x20
#define GPIO0_EN_OFFSET 0x40

#define GPIO4_BIT (4+8)
#define GPIO6_BIT (6+8)
#define GPIO7_BIT (7+8)

int main();
void infinite_func();

void Reset_Handler(void)
{
    main();
    while(1);
}

int main()
{
    unsigned int *gpio0_dir_ptr, *gpio0_do_ptr, *gpio0_en_ptr;
    gpio0_dir_ptr = (unsigned int *)(GRF_BASE_ADDR + GPIO0_DIR_OFFSET);
    gpio0_do_ptr = (unsigned int *)(GRF_BASE_ADDR + GPIO0_DO_OFFSET);
    gpio0_en_ptr = (unsigned int *)(GRF_BASE_ADDR + GPIO0_EN_OFFSET);

    /* Enable output on all GPIO pins */
    *gpio0_dir_ptr = ((1<<GPIO4_BIT)<<16) + (1<<GPIO4_BIT);
    *gpio0_dir_ptr = ((1<<GPIO6_BIT)<<16) + (1<<GPIO6_BIT);

    /* Ouptut 1 to be written to GPIO pins */
    *gpio0_do_ptr  = ((1<<GPIO4_BIT)<<16) + (0<<GPIO4_BIT);
    *gpio0_do_ptr  = ((1<<GPIO6_BIT)<<16) + (0<<GPIO6_BIT);

    /* Enable 1 to be written to GPIO pins */
    *gpio0_en_ptr  = ((1<<GPIO4_BIT)<<16) + (1<<GPIO4_BIT);
    *gpio0_en_ptr  = ((1<<GPIO6_BIT)<<16) + (1<<GPIO6_BIT);

    while (1) {
    }
    return 0x00;
}

void infinite_func()
{
    while(1) {
        /* NOP example */
        asm("mov r0,r0");
    }
}


