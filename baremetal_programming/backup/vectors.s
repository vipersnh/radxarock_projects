.globl _start
_start:
    b skip

skip:
    b master

master:
    mov sp,#0x80000000
    bl _start_code
