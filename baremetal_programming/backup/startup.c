extern int main(int cpu_id, char **);
void _initialize_memory(void);
void infinite_loop(void);
void _start_code();

void _start_code()
{
    register unsigned long cpu_id;
	__asm__ volatile( "mrc p15,0,%0,c0,c0,5" : "=r" (cpu_id) :);
    cpu_id &= 0x03;
    if (cpu_id) {
        infinite_loop();
    } else {
        _initialize_memory();
        main(cpu_id, 0);
        infinite_loop();
    }
}

void _initialize_memory(void)
{
}

void infinite_loop(void)
{
    while (1);
}
