#!python
import sys
sys.path.append("/hdd1/data/projects/vipersnh_git/python_library/make")
import make

profile_main = make.profile_t()

include_dirs = ["./"]
source_dirs = []
source_files = ["vectors.s", "startup.c", "./main.c"]
default_c_cpp_flags = ["-g", "-enum_is_int", "-mcpu=cortex-a9", "-march=armv7-a", "-O0", "-mlittle-endian",
    "-Wall", "-fno-builtin", "-nostdinc", "-nostdlib"]
cflags = default_c_cpp_flags
cxxflags = default_c_cpp_flags
ldflags =  []
linker_scripts = ["cortex_a9.ld"]
libraries = []
arflags = list()

profile_main.name = "main"
profile_main.cc = "arm-none-eabi-gcc"
profile_main.cc_asm = "arm-none-eabi-as"
profile_main.cxx = "arm-none-eabi-g++"
profile_main.ld  = "arm-none-eabi-ld"
profile_main.objcopy = "arm-none-eabi-objcopy"
profile_main.objdump = "arm-none-eabi-objdump"
profile_main.include_dirs = include_dirs
profile_main.source_dirs = source_dirs
profile_main.source_files = source_files
profile_main.cflags = cflags
profile_main.cxxflags = cxxflags
profile_main.ldflags = ldflags
profile_main.linker_scripts = linker_scripts
profile_main.libraries = libraries
profile_main.arflags = arflags
profile_main.executable = "main.elf"
profile_main.library = None
profile_main.verbose_level = make.verbosity.MINIMAL
profile_main.build_dir = "./build"
profile_main.executable_subtargets["iHex"] = [profile_main.objcopy, "-O", "ihex", 
    make.get_build_dir(profile_main)+profile_main.executable, make.get_build_dir(profile_main)+ "main.hex"]
profile_main.executable_subtargets["binary"] = [profile_main.objcopy, "-O", "binary", 
    make.get_build_dir(profile_main)+profile_main.executable, make.get_build_dir(profile_main)+ "main.bin"]
profiles = [profile_main]

make.build(profiles_list = profiles, args = sys.argv[1:])
